package com.mysite.ssb;
public class SavedCat {
    private String catId;
    private String imageUrl;

    public SavedCat(String catId, String imageUrl) {
        this.catId = catId;
        this.imageUrl = imageUrl;
    }

    public String getCatId() {
        return catId;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
