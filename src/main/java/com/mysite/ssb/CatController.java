package com.mysite.ssb;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CatController {

    private WebClient webClient = WebClient.create();
    private List<SavedCat> savedCats = new ArrayList<>();  // 클래스 필드로 선언

    @GetMapping("/cat")
    public String catPage(Model model) {
        List<Cat> cats = getCats();
        model.addAttribute("cats", cats);
        return "cats";  // Thymeleaf 템플릿 파일 이름
    }

    @GetMapping("/api/cats")
    @ResponseBody
    public List<Cat> getCats() {
        List<Cat> cats = webClient.get()
                .uri("https://api.thecatapi.com/v1/images/search?limit=10")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(Cat.class)
                .collectList()
                .block();

        return cats;
    }

    @PostMapping(value = "/savecats", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SavedCat> saveCats(@RequestBody List<String> catIds) {
        List<SavedCat> newlySavedCats = new ArrayList<>();
        for (String catId : catIds) {
            String imageUrl = "https://cdn2.thecatapi.com/images/" + catId;
            String jpgUrl = imageUrl + ".jpg";
            String gifUrl = imageUrl + ".gif";

            try {
                URL url = new URL(jpgUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();

                int code = connection.getResponseCode();

                if (code == 200) {
                    SavedCat savedCatJpg = new SavedCat(catId, jpgUrl);
                    newlySavedCats.add(savedCatJpg);
                }
            } catch (Exception e) {
                System.out.println("Failed to save JPG image with ID " + catId + ": " + e.getMessage());
            }

            try {
                URL url = new URL(gifUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();

                int code = connection.getResponseCode();

                if (code == 200) {
                    SavedCat savedCatGif = new SavedCat(catId, gifUrl);
                    newlySavedCats.add(savedCatGif);
                }
            } catch (Exception e) {
                System.out.println("Failed to save GIF image with ID " + catId + ": " + e.getMessage());
            }
        }

        savedCats.addAll(newlySavedCats);
        return savedCats;
    }

    @RequestMapping("/savezone")
    public String saveZone(Model model) {
        model.addAttribute("savedCats", savedCats);
        return "savezone";
    }

    @PostMapping("/deletecats")
    public String deleteCats(@RequestParam List<String> catIds) {
        savedCats.removeIf(savedCat -> catIds.contains(savedCat.getCatId()));
        return "redirect:/savezone";
    }
}
